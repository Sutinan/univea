<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
	}
	public function index() {
		$this->load->helper('form');
		$this->load->view('contact_email_form');
	}
	public function send_mail() {
		$from_email = "info@univeathailand.com";
		$to_email = $this->input->get("email");
		//Load email library
		$this->load->library('email');
		$config = array();
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'mail.univeathailand.com';
		$config['smtp_user'] = 'info@univeathailand.com';
		$config['smtp_pass'] = 'Hello1234';
		$config['smtp_port'] = 25;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from($from_email, 'Univea Thailand');
		$this->email->to($to_email);
		$this->email->cc("info@univeathailand.com");
		$this->email->subject('Univea Thailand Confirm Registration');
		$mgs = $this->load->view("email_template",'', true);
		$mgs .= date("[h:i:s d/m/Y]") . " End of message.";
		$this->email->message($mgs);
		//Send mail
		if($this->email->send())
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		else
			$this->session->set_flashdata("email_sent","You have encountered an error");
		//redirect("../register/submit.php");
	}

	public function template() {
		$this->load->view("email_template");
	}
}
