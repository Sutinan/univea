<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div
    style="padding:0px 3% 0px 3%;color:rgb(121,121,121);font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;background-color:rgb(245,245,245);min-height:100%;width:94%">
    <table
        style="margin:24px 0px 24px 0px;width:100%;border-collapse:separate">
        <tbody>
        <tr>
            <td class=""
                style="text-align:left">&nbsp;</td>
            <td
                style="border-style:solid;background-color:rgb(255,255,255);border-left-color:rgb(240,240,240);border-bottom-color:rgb(240,240,240);border-right-color:rgb(240,240,240);border-top-color:rgb(240,240,240);border-left-width:1px;border-bottom-width:1px;border-right-width:1px;border-top-width:1px;width:600px;text-align:left">
                <div >
                    <div
                        style="margin:0px auto 0px auto;max-width:600px;width:100%">
                        <div >
                            <div
                                style="margin:0px auto 0px auto;max-width:600px;width:598px">
                                <table align="center" cellspacing="0" cellpadding="0"

                                       style="border-collapse:separate;width:598px;background-color:transparent">
                                    <tbody>
                                    <tr>
                                        <td
                                            style="margin:16px 0px 0px 0px;vertical-align:top;text-align:left;padding-top:16px;margin-top:auto">

                                            <h2
                                                style="margin:0px;font-size:30px;color:#3a3a3a;font-weight:400;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height:1.05;letter-spacing:-0.01em;text-align:center">
                                                <br><font style="color:#3a3a3a;font-size:24px"
                                                ><span
                                                        style="font-weight:bolder">Thank you for registration.</span></font>
                                            </h2></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div >
                            <div
                                style="margin:0px auto 0px auto;max-width:600px;width:598px">
                                <table align="center" cellspacing="0" cellpadding="0" border="0"

                                       style="border-collapse:separate;width:598px;background-color:transparent">
                                    <tbody>
                                    <tr>
                                        <td width="100%"
                                            style="padding:0px 20px 0px 20px;vertical-align:top;text-align:left;margin-top:auto">
                                            <p style="margin:0px 0 1rem 0;color:rgb(121,121,121);font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;text-align:justify;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <br></p>
                                            <p style="margin:0px 0 1rem 0;color:rgb(121,121,121);font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;text-align:justify;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <br></p>
                                            <p dir="ltr"
                                               style="margin:0px 0 1rem 0;color:rgb(121,121,121);font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;line-height:1.38;margin-top:0pt;margin-bottom:0pt">
                                                <span
                                                    style="font-size:11pt;font-family:Arial;color:#3a3a3a;background-color:transparent;vertical-align:baseline;white-space:pre-wrap">We’re exciting to share our information as the </span><span
                                                    style="font-size:11pt;font-family:Arial;color:#3a3a3a;background-color:transparent;vertical-align:baseline;white-space:pre-wrap"><span
                                                        style="font-weight:bolder">Brochure</span></span><span
                                                    style="font-size:11pt;font-family:Arial;color:#3a3a3a;background-color:transparent;vertical-align:baseline;white-space:pre-wrap"> below !</span>
                                            </p>
                                            <p style="color:#bda066;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:18px;font-weight:700;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                Cytocare
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/Cytocare-1.pdf" target="_blank" style="color: #3a3a3a; "> Cytocare คืออะไร </a>
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/Cytocare-2.pdf" target="_blank" style="color: #3a3a3a; "> Cytocare Clinical Tested </a>
                                            </p>

                                            <p style="color:#bda066;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:18px;font-weight:700;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                Dermedics
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/Dermedics-products-catalog-1.png" target="_blank" style="color: #3a3a3a; "> Dermedics Products Catalog #1 </a>
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/Dermedics-products-catalog-2.png" target="_blank" style="color: #3a3a3a; "> Dermedics Products Catalog #2</a>
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/Dermedics-product-review.png" target="_blank" style="color: #3a3a3a; "> Dermedics Products Review</a>
                                            </p>
                                            <p style="color:#bda066;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:18px;font-weight:700;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                Clapio - Cell
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/clapio-cell-sodium-dna.jpg" target="_blank" style="color: #3a3a3a; "> Sodium DNA คืออะไร </a>
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/clapio-cell-คืออะไร.jpg" target="_blank" style="color: #3a3a3a; "> Clapio - Cell คืออะไร </a>
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/clapio-cell-how-to-use.jpg" target="_blank" style="color: #3a3a3a; "> วิธีใช้ Clapio - Cell </a>
                                            </p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                <a href="https://www.univeathailand.com/brochure/clapio-cel-admin-mode.jpg" target="_blank" style="color: #3a3a3a; "> Clapio - Cell for Mode of Administration </a>
                                            </p>
                                            <p style="margin:0px 0 1rem 0;color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;text-align:justify;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <br></p>
                                            <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <img src="https://www.univeathailand.com/brochure/univea-thailand.jpg" style="width: 100%;">
                                            </p>
                                            
                                            <p style="margin:0px 0 1rem 0;color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;text-align:center;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <br> Please do not hesitate to keep in touch with us.<br> We’re more than happy to hear from you.<br></p>
                                            <p style="color:rgb(0,0,0);font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-top:20px;margin-right:15px;margin-bottom:20px;margin-left:15px;text-align: center">
                                                <a href="http://line.me/ti/p/%40zmj2889j" target="_blank" style="border: 10px;padding: 15px 20px 15px 20px;font-size: 20px;color: white; background-color: #e19a63; border-radius: 10px;text-decoration: none;"> Contact Now </a>
                                            </p>
                                            <p style="margin:0px 0 1rem 0;color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;text-align:justify;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <br></p>
                                            <p style="margin:0px 0 1rem 0;color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;text-align:justify;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                <br></p>

                                            <table>
                                                <tr>
                                                    <td style="vertical-align: top">
                                                        <p style="color:#bda066;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:16px;font-weight:700;margin-right:0px;margin-bottom:10px;margin-left:0px">
                                                            Contact Us
                                                        </p>
                                                        <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                            <span style="font-weight: 700;color:#3a3a3a;">website : </span> <a href="https://www.univeathailand.com" target="_blank" style="color:#3a3a3a;text-decoration: none;">www.univeathailand.com</a>
                                                        </p>
                                                        <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                            <span style="font-weight: 700;color:#3a3a3a;">&nbsp;&nbsp;&nbsp;&nbsp;email : </span> <a href="mailto:info@univeathailand.com" target="_blank" style="color:#3a3a3a;text-decoration: none;">info@univeathailand.com</a>
                                                        </p>
                                                        <p style="color:#3a3a3a;font-family:-apple-system,HelveticaNeue,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;font-size:14px;margin-right:0px;margin-bottom:10px;margin-left:15px">
                                                            <span style="font-weight: 700;color:#3a3a3a;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tel : </span> <a href="tel:0959242497" style="color:#3a3a3a; text-decoration: none;" target="_blank">095-924-2497</a>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <img src="https://www.univeathailand.com/brochure/univea-line-offical.png" style="width: 150px;border: 5px solid #00b900; margin-left: 100px; border-radius: 10px;">
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%"
                                            style="padding:0px 20px 0px 20px;vertical-align:top;text-align:left;margin-top:auto">
                                            <br></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td class="m_-577213550032322971o_mail_no_resize m_-577213550032322971o_not_editable"
                style="text-align:left">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
