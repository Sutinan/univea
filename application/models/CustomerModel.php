<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerModel extends CI_Model
{

    public function addCustomer($data)
    {
        $this->db->insert("customer", $data);
        return $this->db->insert_id();
    }
}
