<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiModel extends CI_Model
{

    public function searchLocation($key, $type)
    {
        if ($type == 1)
            $this->db->like("districts.districts_name_th", $key, "both");
        else if ($type ==2) {
            $this->db->like("amphures.amphures_name_th", $key, "both");
        } else if ($type == 3) {
            $this->db->like("provinces.province_name_th", $key, "both");
        }
        $this->db->select("districts.districts_id, districts.districts_name_th as districts_name, districts.zip_code, amphures.amphures_id, amphures.amphures_name_th as amphures_name, provinces.province_id, provinces.province_name_th as province_name ");
        $this->db->join("amphures", "districts.amphure_id=amphures.amphures_id");
        $this->db->join("provinces", "provinces.province_id=amphures.province_id");

        $this->db->limit(10, 0);

        return $this->db->get("districts")->result_object();
    }
}
