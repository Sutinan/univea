$(document).ready(function () {
    var $w = $(window);
    $(window).scroll(function(event) {
        var st = $w.scrollTop();
        if (st > 100 ) {
            $(".navbar-default").css({"background-color": "white", 'box-shadow': '-2px 2px 2px 2px rgba(0,0,0,0.43)'});
            $(".navbar-default .navbar-nav>li>a").css({ "color" : "#7a7777"});
        } else if ($w.width() < 767 && $(".navbar-toggle").attr('aria-expanded') == "true") {
            $(".navbar-default").css({
                "background-color": "#b40902",
                'box-shadow': '-2px 2px 2px 2px rgba(0,0,0,0.43)'
            });
            $(".navbar-default .navbar-nav>li>a").css({ "color" : "white"});
        }
        else {
            $(".navbar-default").css({"background-color": "transparent", 'box-shadow': 'none'});
            $(".navbar-default .navbar-nav>li>a").css({ "color" : "white"});
        }
    });
    var flat = 0;
    $(window).resize(function () {
        if ($w.width() < 767 && flat == 0) {
            $("section#vlinea-info .container .row:last-child div:last-child").insertBefore( $("section#vlinea-info .container .row:last-child div:last-child").prev("div"));
            flat = 1;
        }

        if ($w.width() >= 767 && flat == 1) {
            $("section#vlinea-info .container .row:last-child div:last-child").insertBefore( $("section#vlinea-info .container .row:last-child div:last-child").prev("div"));
            flat = 0
        }
    });

    if ($w.width() < 767 && flat == 0) {
        $("section#vlinea-info .container .row:last-child div:last-child").insertBefore( $("section#vlinea-info .container .row:last-child div:last-child").prev("div"));
        flat = 1;
    }

    if ($w.width() >= 767 && flat == 1) {
        $("section#vlinea-info .container .row:last-child div:last-child").insertBefore( $("section#vlinea-info .container .row:last-child div:last-child").prev("div"));
        flat = 0
    }

    var check = true;
    $("a.search").click(function(){
        if (check) {
            $(".navbar-default .box-search").slideDown(200);
            check = !check;
        } else {
            $(".navbar-default .box-search").slideUp(200)
            check = !check;
        }
    })



    $(".navbar-toggle").click(function(){
        if ($(this).attr('aria-expanded') == "false") {
            var st = $w.scrollTop();
            if (st < 100 ) {
                if ($w.width() < 767) {
                    $(".navbar-default").css({
                        "background-color": "#b40902",
                        'box-shadow': '-2px 2px 2px 2px rgba(0,0,0,0.43)'
                    });
                }
            } else {
                $(".navbar-default").css({"background-color": "white", 'box-shadow': '-2px 2px 2px 2px rgba(0,0,0,0.43)'});
                $(".navbar-default .navbar-nav>li>a").css({ "color" : "#7a7777"});
            }
        } else {
            $(".navbar-default").css({"background-color": "transparent", 'box-shadow': 'none'});
            $(".navbar-default .navbar-nav>li>a").css({ "color" : "white"});
        }
    })

})