// Example #3

var setValue = function(selected) {
    console.log(selected);
    $("#district").val(selected.districts_name);
    $("#amphoe").val(selected.amphures_name);
    $("#province").val(selected.province_name);
    $("#zipcode").val(selected.zip_code);
    $("#district_id").val(selected.districts_id);
    $("#amphures_id").val(selected.amphures_id);
    $("#province_id").val(selected.province_id);
}

var districtsSearch = function () {
    var custom = new Bloodhound({
        datumTokenizer: function(d) { return d.tokens; },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: $('meta[name=base_url]').attr("content") + 'Api/searchLocation?key=%QUERY&type=1',
            wildcard: '%QUERY'
        }
    });

    custom.initialize();


    $('#district').typeahead(null, {
        name: 'search_company',
        displayKey: 'districts_name',
        highlight: true,
        source: custom.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile([
                '<div class="media">',
                '<div class="media-body">',
                '<p>{{districts_name}} > {{amphures_name}} > {{province_name}} > {{zip_code}}</p>',
                '</div>',
                '</div>',
            ].join(''))
        }
    }).on('typeahead:selected', function(event, selection) {
        setValue(selection);
    });
}

var amphuresSearch = function () {
    var custom = new Bloodhound({
        datumTokenizer: function(d) { return d.tokens; },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: $('meta[name=base_url]').attr("content") + 'Api/searchLocation?key=%QUERY&type=2',
            wildcard: '%QUERY'
        }
    });

    custom.initialize();


    $('#amphoe').typeahead(null, {
        name: 'search_company',
        displayKey: 'amphures_name',
        highlight: true,
        source: custom.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile([
                '<div class="media">',
                '<div class="media-body">',
                '<p>{{districts_name}} > {{amphures_name}} > {{province_name}} > {{zip_code}}</p>',
                '</div>',
                '</div>',
            ].join(''))
        }
    }).on('typeahead:selected', function(event, selection) {
        setValue(selection);
    });
}

var provinceSearch = function () {
    var custom = new Bloodhound({
        datumTokenizer: function(d) { return d.tokens; },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: $('meta[name=base_url]').attr("content") + 'Api/searchLocation?key=%QUERY&type=3',
            wildcard: '%QUERY'
        }
    });

    custom.initialize();


    $('#province').typeahead(null, {
        name: 'search_company',
        displayKey: 'province_name',
        highlight: true,
        source: custom.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile([
                '<div class="media">',
                '<div class="media-body">',
                '<p>{{districts_name}} > {{amphures_name}} > {{province_name}} > {{zip_code}}</p>',
                '</div>',
                '</div>',
            ].join(''))
        }
    }).on('typeahead:selected', function(event, selection) {
        setValue(selection);
    });
};

districtsSearch();
amphuresSearch();
provinceSearch();
