<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js"></script>

    <!-- star-rating -->
    <script src="js/jquery.barrating.min.js"></script>
    <link rel="stylesheet" href="css/star-rating-svg.css">

        <!-- jquery.Thailand.js -->
    <script type="text/javascript"
            src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript"
            src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
    <link rel="stylesheet"
          href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
    <script type="text/javascript"
            src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <title>Register a complete of certification</title>

    <link rel="stylesheet" href="https://unpkg.com/bs-stepper/dist/css/bs-stepper.min.css">
    <script src="https://unpkg.com/bs-stepper/dist/js/bs-stepper.min.js"></script>

    <style type="text/css">
        @charset "UTF-8";
        @font-face {
            font-family: 'sukhumvit';
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: "Thai Sans Neue";
            src: url("../assets/fonts/ThaiSansNeue-Regular.ttf") format("truetype");
        }

        body, h1, h2, h3, h4, h5, p, span, li, a, a.link-readmore {
            font-family: "sukhumvit", "Thai Sans Neue";
            color: #7a7777;
        }

        h1 {
            font-size: 1.5rem;
        }

        label {
            font-size: 22px;
        }

        body {
            background-image: linear-gradient(#f6d0a5, #fff);
            background-repeat: no-repeat;
        }

        button.btn-primary, .active .bs-stepper-circle {
            background-color: #7f5c1c !important;
            border-color: #7f5c1c !important;
        }

        .container {
            min-height: 100%;
        }

        iframe {
            border: 0;
            width: 100%;
            height: 200px;
        }

        input.form-control {
            font-size: 22px;
        }

        input, .card {
            background-color: #eee !important;
            border-radius: .5rem !important;
            box-shadow: inset 0 2px 10px rgba(0, 0, 0, .2), 0 -1px 1px #FFF, 0 1px 0 #FFF;

        }

        @media (max-width: 520px) {
            .bs-stepper .step-trigger {
                -ms-flex-direction: column;
                flex-direction: column;
                padding: 2px;
            }
        }
        
    </style>
    <script type="application/javascript">
        stepper1 = null;
        document.addEventListener('DOMContentLoaded', function () {
            stepper1 = new Stepper(document.querySelector('#stepper1'))
        })
    </script>
</head>
<body>

<div class="container" style="padding-bottom: 12rem!important;">
    <div class="d-flex justify-content-center">
    <img src="univea_logo.png" style="width: 140px;height: 140px;margin-top: 40px;">
</div>
    <div class="row">
        <div class="col">
            <div class="d-flex justify-content-center" style="margin-top: 20px;">
                <h1 class="display-2 m-0" style="color: #b3b2b2;font-weight: 500;font-size: 1.5rem;">REGISTER FOR CERTIFICATION</h1>
            </div>
            <div class="d-flex justify-content-center" style="margin-top: 10px;">
                <h1 class="display-2 m-0" style="color: #b3b2b2;font-weight: 500;font-size: 1.5rem;text-align: center"><i>Academic Hands-on Aesthetic treatments, Filler, Thread lift and more May, 2019</i></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div id="stepper1" class="bs-stepper">
                <div class="bs-stepper-header">

                    <div class="step" data-target="#test-l-1">
                        <button type="button" class="btn step-trigger">
                            <span class="bs-stepper-circle">1</span>
                            <span class="bs-stepper-label">First step</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#test-l-2">
                        <button type="button" class="btn step-trigger">
                            <span class="bs-stepper-circle">2</span>
                            <span class="bs-stepper-label">Second step</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#test-l-3">
                        <button type="button" class="btn step-trigger">
                            <span class="bs-stepper-circle">3</span>
                            <span class="bs-stepper-label">Third step</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#test-l-4">
                        <button type="button" class="btn step-trigger">
                            <span class="bs-stepper-circle">4</span>
                            <span class="bs-stepper-label">last step</span>
                        </button>
                    </div>
                </div>
                <div class="bs-stepper-content">
                    <form method="POST" action="submit.php">
                        <div id="test-l-1" class="content ">
                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>First name</label>
                                        <input id="first_name" type="text" class="form-control" name="first_name"
                                               placeholder="Enter first Name">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Last name</label>
                                        <input type="text" class="form-control" name="last_name"
                                               placeholder="Enter last Name">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Date of Birth</label>
                                        <input id="datepicker" class="form-control" name="birth_date"
                                               placeholder="Choose date of birth">
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3">
                                <button type="button" class="btn btn-lg btn-primary float-right"
                                        onclick="stepper1.next()">
                                    Next
                                </button>
                            </div>
                        </div>
                        <div id="test-l-2" class="content">

                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Phone number</label>
                                        <input type="text" class="form-control" name="phone_number"
                                               placeholder="Enter phone number">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input id="email" type="email" class="form-control" name="email"
                                               placeholder="Enter email address">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>เลขที่ใบประกอบวิชาชีพ</label>
                                        <input type="text" class="form-control" name="medical_license"
                                               placeholder="Enter medical license number">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between mt-3">
                                <button type="button" class="btn btn-lg btn-primary" onclick="stepper1.previous()">
                                    Previous
                                </button>
                                <button type="button" class="btn btn-lg btn-primary" onclick="stepper1.next()">Next
                                </button>
                            </div>
                        </div>
                        <div id="test-l-3" class="content">

                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address"
                                       placeholder="Enter current address">
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Tumbon</label>
                                        <input id="district" class="form-control" name="district"
                                               placeholder="Enter district">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Amphoe</label>
                                        <input id="amphoe" type="text" class="form-control" name="amphoe"
                                               placeholder="Enter amphoe">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>Province</label>
                                        <input id="province" class="form-control" name="province"
                                               placeholder="Enter province">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label> Zipcode</label>
                                        <input id="zipcode" type="text" class="form-control" name="zipcode"
                                               placeholder="Enter zipcode">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md">
                                    <h3>Are you currently working or used to work in beauty/surgery clinic?</h3>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="work_clinic"
                                               id="inlineRadio1"
                                               value="yes">
                                        <label class="form-check-label" for="inlineRadio1" >Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="work_clinic"
                                               id="inlineRadio2"
                                               value="no">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="clinic_box" style="display: none;">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <label>What's clinic name?</label>
                                        <input type="text" class="form-control" name="clinic_name"
                                               placeholder="Enter clinic name">
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex justify-content-between mt-3">
                                <button type="button" class="btn btn-lg btn-primary" onclick="stepper1.previous()">
                                    Previous
                                </button>
                                <button type="button" class="btn btn-lg btn-primary" onclick="stepper1.next()">Next
                                </button>
                            </div>
                        </div>
                        <div id="test-l-4" class="content">
                            
                             <div class="row">
                                <div class="col-sm-12 col-md">
                                    <h3>Could you please rate this course</h3>
                                    <div class="stars stars-example-css">
                                        <select id="star-css" name="rating" autocomplete="off">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group">
                                        <h3 for="comment">Please leave us a review</h3>
                                        <textarea style="font-size: 22px;" class="form-control" name="comment" id="comment" rows="3" placeholder="ex. open more courses, add extra learning time, giving more hands-on cases, demonstrate product variety." required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mt-3">
                                <button type="submit" name="submit" value="true" class="btn btn-lg btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    bootstrapValidate(
        '#email',
        'email:Enter a valid E-Mail Address!'
    );
    bootstrapValidate('#first_name', 'required:Please fill out this field!')

    $.Thailand({
        $district: $('#district'),
        $amphoe: $('#amphoe'),
        $province: $('#province'),
        $zipcode: $('#zipcode'),
    })

    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd/mm/yyyy',
    });

    $('[name="work_clinic"]').click(function (e) {
        var option = $(this).attr('value');
        if (option == 'yes') {
            $('#clinic_box').show();
        } else {
            $('#clinic_box').hide();
        }
    });

    $(function() {
        $('#star-css').barrating({
            theme: 'css-stars',
            showSelectedRating: false
        });
    });

</script>

</body>
</html>