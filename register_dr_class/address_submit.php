<?php 
require_once 'config.php';
require_once 'Register.php';

if(isset($_POST['submit']) && !empty($_POST['submit'])){
	$register = new Register();
	$register->email = $_POST['email'];
	$register->address = $_POST['address'];
	$register->district = $_POST['district'];
	$register->amphoe = $_POST['amphoe'];
	$register->province = $_POST['province'];
	$register->zipcode = $_POST['zipcode'];
	$register->saveAddress();
}
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
	  <meta http-equiv="Content-Security-Policy" content="block-all-mixed-content" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	  <title>Register Univea Thailand</title>

    <style type="text/css">
        body {
            background-image: linear-gradient(#f6d0a5, #fff);
            background-repeat: no-repeat;
        }
		.container {
		    height: 900px;
		}
    </style>
  </head>
<body>
	<div class="container py-5">
		<div class="row">
	    	<div class="col">
	    		<div class="d-flex justify-content-center" style="margin-top: 25px;">
		    		<h1 class="display-2 m-0" style="color: #79561b;font-weight: 500;font-size: 3rem;">THANK YOU</h1>
		    	</div>	
		    	<div class="d-flex justify-content-center">
		    		<h1 class="display-2 m-0" style="color: #79561b;font-weight: 500;font-size: 1.5rem;text-align: center;padding: 46px 0;">We're hoping you make the most out of the previous course and We're looking forward to see you on our upcoming lesson</h1>
		    	</div>
		    </div>
		</div>
	</div>
</body>
</html>