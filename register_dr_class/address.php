<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- jquery.Thailand.js -->
    <script type="text/javascript"
            src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript"
            src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
    <link rel="stylesheet"
          href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
    <script type="text/javascript"
            src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Univea Thailand Group</title>


    <style type="text/css">
        @charset "UTF-8";
        @font-face {
            font-family: 'sukhumvit';
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: "Thai Sans Neue";
            src: url("../assets/fonts/ThaiSansNeue-Regular.ttf") format("truetype");
        }

        body, h1, h2, h3, h4, h5, p, span, li, a, a.link-readmore {
            font-family: "sukhumvit", "Thai Sans Neue";
            color: #7a7777;
        }

        button.btn-primary{
            background-color: #7f5c1c !important;
            border-color: #7f5c1c !important;
        }


        h1 {
            font-size: 1.5rem;
        }

        label {
            font-size: 22px;
        }

        body {
            background-image: linear-gradient(#f6d0a5, #fff);
            background-repeat: no-repeat;
        }

        iframe {
            border: 0;
            width: 100%;
            height: 200px;
        }

        input.form-control {
            font-size: 22px;
        }

        input, .card {
            background-color: #eee !important;
            border-radius: .5rem !important;
            box-shadow: inset 0 2px 10px rgba(0, 0, 0, .2), 0 -1px 1px #FFF, 0 1px 0 #FFF;

        }
    </style>
</head>
<body>
<div class="container py-5">
	<div class="container" style="padding-bottom: 12rem!important;">
	    <div class="d-flex justify-content-center">
	    <img src="univea_logo.png" style="width: 140px;height: 140px;margin-top: 40px;">
	</div>
    <div class="row pb-3">
        <div class="col">
            <div class="d-flex justify-content-center" style="margin-top: 20px;">
                <h1 class="display-2 m-0" style="color: #b3b2b2;font-weight: 500;font-size: 1.5rem;"></h1>
            </div>
            <div class="d-flex justify-content-center" style="margin-top: 10px;">
                <h1 class="display-2 m-0" style="color: #b3b2b2;font-weight: 500;font-size: 1.5rem;text-align: center"><i>Academic Hands-on Aesthetic treatments, Filler, Thread lift and HIFU 27 - 28 April, 2019</i></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
	    	<form method="POST" action="address_submit.php">
	            <div id="test-l-3" class="content">
	               
	                <div class="row">
	                    <div class="col-sm-12 col-md">
	                        <div class="form-group">
	                            <label>First name</label>
	                            <input class="form-control" name="first_name" value="<?=$_GET['first_name']?>">
	                        </div>
	                    </div>
	                    <div class="col-sm-12 col-md">
	                        <div class="form-group">
	                            <label>Last name</label>
	                            <input type="text" class="form-control" name="last_name" value="<?=$_GET['last_name']?>">
	                        </div>
	                    </div>
	                </div>

	                <div class="form-group">
	                    <label>Address</label>
	                    <input type="text" class="form-control" name="address"
	                           placeholder="Enter current address">
	                </div>

	                <div class="row">
	                    <div class="col-sm-12 col-md">
	                        <div class="form-group">
	                            <label>district</label>
	                            <input id="district" class="form-control" name="district"
	                                   placeholder="Enter district">
	                        </div>
	                    </div>
	                    <div class="col-sm-12 col-md">
	                        <div class="form-group">
	                            <label>Amphoe</label>
	                            <input id="amphoe" type="text" class="form-control" name="amphoe"
	                                   placeholder="Enter amphoe">
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12 col-md">
	                        <div class="form-group">
	                            <label>Province</label>
	                            <input id="province" class="form-control" name="province"
	                                   placeholder="Enter province">
	                        </div>
	                    </div>
	                    <div class="col-sm-12 col-md">
	                        <div class="form-group">
	                            <label>Zipcode</label>
	                            <input id="zipcode" type="text" class="form-control" name="zipcode"
	                                   placeholder="Enter zipcode">
	                        </div>
	                    </div>
	                </div>

	                <div class="d-flex justify-content-center mt-3">
	                	<input type="hidden" class="form-control" name="email" value="<?=$_GET['email']?>">
	                    <button type="submit" name="submit" value="true" class="btn btn-lg btn-primary">Save</button>
	                </div>
	            </div>
	        </form>
        </div>
    </div>
</div>

<script>
    $.Thailand({
        $district: $('#district'),
        $amphoe: $('#amphoe'),
        $province: $('#province'),
        $zipcode: $('#zipcode'),
    })
</script>

</body>
</html>