<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

class Register {

	public $db;
	public $first_name;
	public $last_name;
	public $birth_date;
	public $phone_number;
	public $email;
	public $clinic_name;
	public $address;
	public $district;
	public $amphoe;
	public $province;
	public $zipcode;

	public function __construct(){

		try {
			$this->db = new PDO("mysql:host=". HOST_NAME .";dbname=" . DATABASE_NAME, USER_NAME, PASSWORD,[
			    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
			    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
			    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			]);
		}catch (PDOException $e){
			echo "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}

	public function getRegister() {
		return $this->db->query("SELECT * FROM register_univea")->fetch();
	}

	public function save() {
		$this->db->prepare("INSERT INTO `register_univea` (`first_name`, `last_name`, `birth_date`, `phone_number`, `email`, `clinic_name`, `address`, `district`, `amphoe`, `province`, `zipcode`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
		->execute([
			$this->first_name,
			$this->last_name,
			date('Y-m-d', strtotime(str_replace('/', '-', $this->birth_date))),
			$this->phone_number,
			$this->email,
			$this->clinic_name,
			$this->address,
			$this->district,
			$this->amphoe,
			$this->province,
			$this->zipcode,
		]);
		
	}
}